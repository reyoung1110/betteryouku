package me.reyoung.BetterYouku;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import me.reyoung.BetterYouku.parser.*;
import me.reyoung.BetterYouku.parser.youku.YoukuParser;
import me.reyoung.BetterYouku.util.AsyncResult;

import java.net.MalformedURLException;
import java.net.URL;

public class MainActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    final String LOG_TAG = String.format("BY_%s",this.getClass().getSimpleName());
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Button testBtn = (Button) this.findViewById(R.id.test_btn);
        testBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                YoukuParser youkuParser = new YoukuParser();
                ParserTask task = new ParserTask(youkuParser){

                    @Override
                    protected void onPostExecute(AsyncResult<IParseResult, ParseException>[] asyncResults) {
                        super.onPostExecute(asyncResults);
                        for (AsyncResult<IParseResult, ParseException> result: asyncResults){
                            if(result.Exception==null){
                                Log.d(LOG_TAG,String.format("Parse Complete %s",result.Result.getTitle()));
                            }
                        }
                    }
                };
                try {
                    task.execute(new IParser.ParserParam("http://v.youku.com/v_show/id_XNzE0Mzg2ODI4_ev_1.html",VideoDefinition.NORMAL));
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
//                Log.d(LOG_TAG, id);
            }
        });
    }
}
