package me.reyoung.BetterYouku.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 上午12:40
 */
public class HttpUtil {
    public static String retirePage(String urlstr) throws IOException {
        HttpURLConnection conn = null;
        String retv = null;
        URL url = new URL(urlstr);
        try {
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            StringBuffer sb = new StringBuffer();
            int status = conn.getResponseCode();
            if(status>=200&&status<400){
                InputStreamReader reader = new InputStreamReader(
                        conn.getInputStream()
                );
                char[] buff = new char[1024];
                int sz;
                do{
                    sz = reader.read(buff);
                    if(sz!=-1){
                        sb.append(buff,0,sz);
                    }
                } while (sz!=-1);
                retv = sb.toString();
            }
        }finally {
            if(conn!=null){
                conn.disconnect();
            }
        }
        return  retv;
    }

    public static JSONObject retirePageJson(String url) throws IOException, JSONException {
        String data = retirePage(url);
        JSONObject object = (JSONObject) new JSONTokener(data).nextValue();
        return object;
    }
}
