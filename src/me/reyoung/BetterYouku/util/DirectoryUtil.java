package me.reyoung.BetterYouku.util;

import android.os.Environment;

import java.io.File;
import java.io.FileFilter;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午1:39
 */
public class DirectoryUtil {

    public static File getAppDir(){
        File sdCard = Environment.getExternalStorageDirectory();
        File[] dirs = sdCard.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.isDirectory() && file.getName().equals("BYK");
            }
        });
        if(dirs.length==1){
            return dirs[0];
        } else {
            File f = new File(sdCard.getAbsolutePath() + File.separator + "BYK/");
            boolean ok = f.mkdir();
            return f;
        }
    }

    public static File getAppDir(String sub){
        File app = getAppDir();
        File subDir = new File(app.getAbsolutePath() + File.separator + sub);
        if(!subDir.exists()||!subDir.isDirectory()){
            subDir.mkdirs();
        }
        return subDir;
    }
    public static File getDownloadFileName(String album, int vid, int idx, String extName) {
        File dir = DirectoryUtil.getAppDir(String.format("%s/%d/", album, vid));
        return new File(dir.getAbsolutePath() + File.separator +String.format("%d.%s",idx, extName));
    }
}
