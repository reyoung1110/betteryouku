package me.reyoung.BetterYouku.util;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 上午1:43
 */
public class AsyncResult<ResultType, ExceptionType extends Throwable> {
    public ResultType Result;
    public ExceptionType Exception;
}
