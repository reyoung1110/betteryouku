package me.reyoung.BetterYouku.service;

import android.app.IntentService;
import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;
import android.util.Log;
import fj.Effect;
import me.reyoung.BetterYouku.persist.sql.DBHelper;
import me.reyoung.BetterYouku.util.DirectoryUtil;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午6:49
 */
public class DownloadService extends IntentService {

    private static final String LOG_TAG = "DownloadService";

    public static final String DownloadServiceNotifyKey  = "me.reyoung.BetterYouku.DownloadService_notify_key";

    private final ConcurrentLinkedQueue<Integer> downloadQueue = new ConcurrentLinkedQueue<Integer>();

    private DownloadThread thread = new DownloadThread();

    public DownloadService() {
        super("DownloadService");
    }

    private class DownloadThread extends Thread{
        private class DownloadParam{
            public int idx;
            public String url;
            public String album;
            public String title;
            public int vid;
            public String extName;
        }

        @Override
        public void run() {
            while(true){
                final Integer id = downloadQueue.peek();
                if(id==null){
                    for(int i=0;i<60;++i){
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            break;
                        }
                    }
                    refreshQueue();
                } else {
                    final DownloadParam p = getDownloadParamById(id);
                    downloadFile(p);
                    new DBHelper(DownloadService.this).doWrite(new Effect<SQLiteDatabase>() {
                        @Override
                        public void e(SQLiteDatabase database) {
                            ContentValues cv = new ContentValues();
                            cv.put("isDownloaded", 1);
                            database.update("VideoItem", cv,"id = ?", new String[]{
                                    String.valueOf(id)
                            });
                        }
                    });
                    downloadQueue.remove();
                    Intent intent = new Intent(DownloadServiceNotifyKey);
                    sendBroadcast(intent);
                }

            }
        }

        private boolean downloadFile(DownloadParam p) {
            File videoFile = DirectoryUtil.getDownloadFileName(p.album, p.vid, p.idx, p.extName);
            Log.d(LOG_TAG, videoFile.getAbsolutePath());
            BufferedInputStream bin = null;
            FileOutputStream fout = null;
            try {
                videoFile.createNewFile();
                fout = new FileOutputStream(videoFile);
                byte [] buffer = new byte[1024];
                URL url = new URL(p.url);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                bin = new BufferedInputStream(conn.getInputStream());
                while(true){
                    int sz = bin.read(buffer);
                    if(sz==-1){
                        break;
                    }
                    fout.write(buffer,0,sz);
                }

            } catch (Throwable ignored){
                return false;
            }finally {
                if(bin!=null){
                    try {
                        bin.close();
                    } catch (IOException e) {
                    }
                }
                if(fout!=null){
                    try {
                        fout.close();
                    } catch (IOException e) {
                    }
                }
            }
            Log.d(LOG_TAG,"Download Complete");
            return true;
        }



        private DownloadParam getDownloadParamById(final Integer id) {
            final DownloadParam p = new DownloadParam();
            new DBHelper(DownloadService.this).doRead(new Effect<SQLiteDatabase>() {
                @Override
                public void e(SQLiteDatabase database) {
                    Cursor cursor = database.rawQuery("SELECT vi.idx, vi.videoUrl, v.album_name, v.title, v.id, vi.fileExtName FROM Video v JOIN VideoItem vi ON v.id = vi.video_id " +
                            "WHERE vi.id = ? LIMIT 1", new String[]{
                            String.valueOf(id)
                    });
                    for (;cursor.moveToNext();){
                        p.idx = cursor.getInt(0);
                        p.url = cursor.getString(1);
                        p.album = cursor.getString(2);
                        p.title = cursor.getString(3);
                        p.vid = cursor.getInt(4);
                        p.extName = cursor.getString(5);
                    }
                }
            });
            return p;
        }
    }


    @Override
    public void onCreate() {
        super.onCreate();
        refreshQueue();
        thread.start();
    }

    public void refreshQueue(){
        new DBHelper(this).doRead(new Effect<SQLiteDatabase>() {
            @Override
            public void e(SQLiteDatabase database) {
                Cursor cursor = database.rawQuery("SELECT id FROM VideoItem WHERE isDownloaded = 0",null);
                for(;cursor.moveToNext();){
                    int id = cursor.getInt(0);
                    if(!downloadQueue.contains(id)){
                        downloadQueue.add(id);
                    }
                }
            }
        });
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        thread.interrupt();
    }
}
