package me.reyoung.BetterYouku.parser;

import android.os.AsyncTask;
import me.reyoung.BetterYouku.util.AsyncResult;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 上午12:57
 */
public class ParserTask extends AsyncTask<IParser.ParserParam, Integer, AsyncResult<IParseResult,ParseException>[]> {
    private IParser mParser;

    public ParserTask(IParser p){
        mParser = p;
    }


    @Override
    protected AsyncResult<IParseResult,ParseException>[] doInBackground(IParser.ParserParam... parserParams) {
        int cnt = parserParams.length;
        AsyncResult[] retv = new AsyncResult[cnt];
        for(int i=0;i<cnt;++i){
            this.publishProgress(i*100/cnt);
            AsyncResult<IParseResult,ParseException> result = new  AsyncResult<IParseResult,ParseException>();
            try{
                result.Result= mParser.parse(parserParams[i].url, parserParams[i].definition);
            }catch (ParseException e){
                result.Exception = e;
            }
            retv[i] = result;
            this.publishProgress((i+1)*100/cnt);
            if(this.isCancelled()){
                break;
            }
        }
        return retv;
    }
}
