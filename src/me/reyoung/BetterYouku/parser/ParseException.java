package me.reyoung.BetterYouku.parser;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 上午1:40
 */
public class ParseException extends Exception {
    public ParseException() {  }

    public ParseException(java.lang.String detailMessage) {
        super(detailMessage);
    }

    public ParseException(java.lang.String detailMessage, java.lang.Throwable throwable) {
        super(detailMessage,throwable);
    }

    public ParseException(java.lang.Throwable throwable) {
        super(throwable);
    }
}
