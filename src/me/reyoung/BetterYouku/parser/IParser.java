package me.reyoung.BetterYouku.parser;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * User: reyoung
 * Date: 14-5-20
 * Time: 下午11:00
 */
public interface IParser {
    public static class ParserParam {
        public URL url;
        public VideoDefinition definition;
        public ParserParam(){

        }
        public ParserParam(URL url, VideoDefinition definition){
            this.url = url;
            this.definition = definition;
        }
        public ParserParam(String url,VideoDefinition definition) throws MalformedURLException {
            this.url = new URL(url);
            this.definition = definition;
        }
    }
    public IParseResult parse(URL url, VideoDefinition vd) throws ParseException;
}
