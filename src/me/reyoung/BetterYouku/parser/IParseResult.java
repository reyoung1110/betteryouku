package me.reyoung.BetterYouku.parser;

import java.net.URL;
import java.util.Collection;

/**
 * User: reyoung
 * Date: 14-5-20
 * Time: 下午11:00
 */
public interface IParseResult {
    public Collection<URL> getDownloadUrls();
    public String getFileExtName();
    public String getTitle();
    public String getAuthor();
    public Double getSeconds();
    public String getLogoUrl();
}
