package me.reyoung.BetterYouku.parser.youku;

import android.util.Log;
import me.reyoung.BetterYouku.parser.IParseResult;
import me.reyoung.BetterYouku.parser.IParser;
import me.reyoung.BetterYouku.parser.ParseException;
import me.reyoung.BetterYouku.parser.VideoDefinition;
import me.reyoung.BetterYouku.util.HttpUtil;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * User: reyoung
 * Date: 14-5-20
 * Time: 下午11:02
 */
public class YoukuParser implements IParser {
    private final String LogTAG = "YoukuParser";


    public String getVideoID(String url){
        Pattern[] patterns = new Pattern[]{
          Pattern.compile("http://player\\.youku\\.com/player\\.php/sid/([A-Za-z0-9]+)/v\\.swf\\.*"),
          Pattern.compile("http://(v|www)\\.youku\\.com/v_show/id_([A-Za-z0-9]+)(|.+)\\.html.*"),
          Pattern.compile("^loader\\.swf\\?VideoIDS=([A-Za-z0-9]+)"),
          Pattern.compile("^([A-Za-z0-9]+)$")
        };
        int [] patternIdx = {1,2,1,1};
        String urlStr = url;
        String id = null;
        for(int i=0;i<patterns.length&&id==null;++i){
            Pattern p = patterns[i];
            Matcher m = p.matcher(urlStr);
            if (m.matches()){
                Log.d(LogTAG,"Founded");
                Log.d(LogTAG,String.format("Group Count = %d",m.groupCount()));
                try{
                    id = m.group(patternIdx[i]);
                } catch (Throwable ignored){

                }
            }
        }
        return id;
    }



    @Override
    public IParseResult parse(URL url, VideoDefinition vd) throws ParseException {
        String strUrl = url.toString();
        String videoId = getVideoID(strUrl);
        if(videoId!=null){

            try {
                String videoInfoUrl = String.format("http://v.youku.com/player/getPlayList/VideoIDS/%s",videoId);
                Log.d(LogTAG,String.format("VideoInfo Url %s",videoInfoUrl));
                JSONObject videoInfo = HttpUtil.retirePageJson(videoInfoUrl);
                JSONObject data = videoInfo.getJSONArray("data").getJSONObject(0);
                final String videoTitle = data.getString("title");
                final String username = data.optString("username");
                final Double sec = data.optDouble("seconds");
                final String logoUrl = data.optString("logo");

                Log.d(LogTAG, String.format("Video Title %s",videoTitle));

                JSONObject segs = data.getJSONObject("segs");

                // Get Video Definition
                String key = null;
                switch (vd){
                    case NORMAL:
                        key = "flv";
                        break;
                    case HIGH:
                        if(segs.has("mp4")){
                            key = "mp4";
                        } else {
                            key = "flv";
                        }
                        break;
                    case SUPER:
                        if(segs.has("hd2")){
                            key = "hd2";
                        } else if(segs.has("mp4")){
                            key = "mp4";
                        } else {
                            key = "flv";
                        }
                        break;
                }
                Log.d(LogTAG,String.format("Key = %s",key));

                int seed = data.getInt("seed");
                byte[] mixed = new byte["abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/\\:._-1234567890".length()];
                final byte[] sourceByte = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ/\\:._-1234567890".getBytes();
                LinkedList<Byte> source = new LinkedList<Byte>();
                for(byte b : sourceByte){
                    source.add(b);
                }
                int mixedIdx = 0;
                while (!source.isEmpty()){
                    seed = (seed*211+30031)&0xFFFF;
                    int idx = seed*source.size() >> 16;
                    byte ch = source.get(idx);
                    source.remove(idx);
                    mixed[mixedIdx++] = ch;
                }

                String[] streamfileids = data.getJSONObject("streamfileids").getString(key).split("\\*");
                StringBuilder streamfileidsSb = new StringBuilder();
                for(String id:streamfileids){
                    if(!id.isEmpty()){
                        streamfileidsSb.append((char)mixed[Integer.parseInt(id,10)]);
                    }
                }
                String streamFileIdsStr = streamfileidsSb.toString();
                Log.d(LogTAG,String.format("Stream File Id = %s",streamFileIdsStr));

                String vidLow = streamFileIdsStr.substring(0,8);
                String vidHigh = streamFileIdsStr.substring(10);
                final String fileType;
                if(key.equals("mp4")){
                    fileType = "mp4";
                } else {
                    fileType = "flv";
                }
                JSONArray segsUseful = segs.getJSONArray(key);
                final List<URL> resultList = new ArrayList<URL>(segsUseful.length());
                for(int i=0;i<segsUseful.length();++i){
                    JSONObject obj = segsUseful.getJSONObject(i);
                    String no = String.format("%02X",obj.getInt("no"));
                    String result = String.format("http://f.youku.com/player/getFlvPath/sid/00_%s/st/%s/fileid/%s%s%s?K=%s",
                            no, fileType, vidLow, no, vidHigh, obj.getString("k"));
                    Log.d(LogTAG,String.format("DURL %s",result));
                    resultList.add(new URL(result));
                }
                return new IParseResult() {
                    @Override
                    public Collection<URL> getDownloadUrls() {
                        return resultList;
                    }

                    @Override
                    public String getFileExtName() {
                        return fileType;
                    }

                    @Override
                    public String getTitle() {
                        return videoTitle;
                    }

                    @Override
                    public String getAuthor() {
                        return username;
                    }

                    @Override
                    public Double getSeconds() {
                        return sec;
                    }

                    @Override
                    public String getLogoUrl() {
                        return logoUrl;
                    }
                };
            } catch (IOException e) {
                throw new ParseException(e);
            } catch (JSONException e) {
                throw new ParseException(e);
            }
        }
        return null;
    }
}
