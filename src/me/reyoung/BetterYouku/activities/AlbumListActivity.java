package me.reyoung.BetterYouku.activities;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import me.reyoung.BetterYouku.persist.sql.DBHelper;
import me.reyoung.BetterYouku.service.DownloadService;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午12:40
 */
public class AlbumListActivity extends ListActivity {
    private final static String LOG_TAG = "AlbumListActivity";

    private AlbumListViewAdapter.AlbumListItem[] items;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        updateListView();
        Intent serv = new Intent(this, DownloadService.class);
        startService(serv);
    }

    private void updateListView() {
        DBHelper dbHelper = new DBHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        try {
            if (db != null) {
                Cursor cursor = db.query("Album", new String[]{"name", "description", "imageUrl"}, null, null, null, null, "createTime desc");
                Log.d(LOG_TAG, String.format("Album Count = %d", cursor.getCount()));
                items = new AlbumListViewAdapter.AlbumListItem[cursor.getCount()];
                for (int i =0; cursor.moveToNext(); ++i) {
                    items[i] = new AlbumListViewAdapter.AlbumListItem();
                    items[i].name = cursor.getString(0);
                    items[i].desc = cursor.getString(1);
                    items[i].imgUrl= cursor.getString(2);
                }
                setListAdapter(new AlbumListViewAdapter(this, items));
            }
        } finally {
            if (db != null) {
                db.close();
            }
        }
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Log.d(LOG_TAG, String.format("Name %s",items[position].name));
        Intent intent = new Intent(this, VideoListActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("album_name", items[position].name);
        intent.putExtras(bundle);
        startActivity(intent);
        super.onListItemClick(l, v, position, id);
    }
}