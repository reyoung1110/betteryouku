package me.reyoung.BetterYouku.activities;

import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import fj.Effect;
import me.reyoung.BetterYouku.persist.sql.DBHelper;
import me.reyoung.BetterYouku.util.DirectoryUtil;

import java.io.File;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午9:12
 */
public class VideoPieceListActivity extends BaseDownloadStatusActivity {
    private final static String LOG_TAG = "VideoPieceListActivity";
    private int videoId;
    private VideoPieceListViewAdapter.VideoPieceItem[] items;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        videoId = getIntent().getExtras().getInt("video_id");
        updateList(videoId);
    }

    private void updateList(final int videoId) {
        new DBHelper(this).doRead(new Effect<SQLiteDatabase>() {
            @Override
            public void e(SQLiteDatabase database) {
                Cursor cursor = database.rawQuery("SELECT v.title, v.image_url, vi.isDownloaded, vi.idx, vi.fileExtName, " +
                        "v.album_name, v.id FROM Video v JOIN VideoItem vi ON v.id = vi.video_id " +
                        "WHERE v.id = ? ORDER BY vi.idx", new String[]{
                        String.valueOf(videoId)
                });
                items = new VideoPieceListViewAdapter.VideoPieceItem[cursor.getCount()];

                for(int i=0;cursor.moveToNext();++i){
                    items[i] = new VideoPieceListViewAdapter.VideoPieceItem();
                    items[i].title = cursor.getString(0);
                    items[i].picUrl = cursor.getString(1);
                    items[i].isDownloaded = cursor.getInt(2) == 1;
                    items[i].id = cursor.getInt(3);
                    items[i].extName = cursor.getString(4);
                    items[i].albumName = cursor.getString(5);
                    items[i].vid = cursor.getInt(6);
                }
            }
        });
        this.setListAdapter(new VideoPieceListViewAdapter(this,items));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Intent intent = new Intent();
        intent.setAction(android.content.Intent.ACTION_VIEW);
        File file = DirectoryUtil.getDownloadFileName(
                items[position].albumName,items[position].vid,items[position].id,items[position].extName
        );
        intent.setDataAndType(Uri.fromFile(file), "video/*");
        startActivity(intent);
    }

    @Override
    protected void onDownloadServiceStatusChange() {
        updateList(videoId);
    }
}