package me.reyoung.BetterYouku.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fj.Effect;
import me.reyoung.BetterYouku.R;
import me.reyoung.BetterYouku.persist.sql.DBHelper;
import me.reyoung.BetterYouku.persist.sql.dao.VideoDao;
import me.reyoung.BetterYouku.util.DownloadImageTask;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午3:48
 */
public class VideoListViewAdapter extends BaseAdapter {
    private Activity context;
    private VideoDao.VideoItem[] items;

    public VideoListViewAdapter(Activity context,VideoDao.VideoItem[] items){
        this.context = context;
        this.items = items;
    }


    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return items[i].id;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.album_list_item, parent, false);
        assert rowView != null;
        TextView name = (TextView) rowView.findViewById(R.id.name_text);
        name.setText(items[position].title);
        TextView desc = (TextView) rowView.findViewById(R.id.desc_text);
        final StringBuilder descSB = new StringBuilder();
        descSB.append("已下载 ");
        new DBHelper(context).doRead(new Effect<SQLiteDatabase>() {
            @Override
            public void e(SQLiteDatabase database) {
                Cursor cursor = database.rawQuery("SELECT SUM(isDownloaded), COUNT(isDownloaded) FROM VideoItem WHERE video_id = ?",
                        new String[]{String.valueOf(items[position].id)}
                );
                if(cursor.getCount()!=0){
                    cursor.moveToNext();
                    descSB.append(cursor.getLong(0));
                    descSB.append("/");
                    descSB.append(cursor.getLong(1));
                }
            }
        });
        desc.setText(descSB.toString());
        ImageView img = (ImageView) rowView.findViewById(R.id.album_image_view);
        new DownloadImageTask(img).execute(items[position].imgUrl);
        context.registerForContextMenu(rowView);
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putInt("video_id", items[position].id);
                Intent intent = new Intent(context, VideoPieceListActivity.class);
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
        rowView.setId(items[position].id);
        return rowView;
    }
}
