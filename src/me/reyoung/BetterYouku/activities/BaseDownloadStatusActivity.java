package me.reyoung.BetterYouku.activities;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import me.reyoung.BetterYouku.service.DownloadService;

/**
 * User: reyoung
 * Date: 14-5-22
 * Time: 下午4:57
 */
public abstract class BaseDownloadStatusActivity extends ListActivity {
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onDownloadServiceStatusChange();
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(receiver, new IntentFilter(DownloadService.DownloadServiceNotifyKey));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    protected abstract void onDownloadServiceStatusChange();
}
