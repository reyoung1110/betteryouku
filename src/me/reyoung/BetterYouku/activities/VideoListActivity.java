package me.reyoung.BetterYouku.activities;

import android.app.AlertDialog;
import android.content.*;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import fj.Effect;
import me.reyoung.BetterYouku.parser.*;
import me.reyoung.BetterYouku.parser.youku.YoukuParser;
import me.reyoung.BetterYouku.persist.sql.DBHelper;
import me.reyoung.BetterYouku.persist.sql.dao.VideoDao;
import me.reyoung.BetterYouku.service.DownloadService;
import me.reyoung.BetterYouku.util.AsyncResult;

import java.net.MalformedURLException;


/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午3:28
 */
public class VideoListActivity extends BaseDownloadStatusActivity {
    private final static String LOG_TAG = "VideoListActivity";
    private String albumName;
    private MenuItem newVideoItem;
    private VideoDao.VideoItem[] items;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item == newVideoItem){
            Log.d(LOG_TAG,"New Video Clicked");
            askString("请输入Youku视频网址", new Effect<String>() {
                @Override
                public void e(String s) {
                    try {
                        addNewVideo(s);
                    } catch (MalformedURLException e) {
                        Toast.makeText(VideoListActivity.this,"输入URL错误",Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
        return true;
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, final View v, ContextMenu.ContextMenuInfo menuInfo) {
        menu.setHeaderTitle("文件操作");
        menu.add(0, 1, Menu.NONE, "删除").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Log.d(LOG_TAG,String.format("Item Menued %d",v.getId()));
                new DBHelper(VideoListActivity.this).doWrite(new Effect<SQLiteDatabase>() {
                    @Override
                    public void e(SQLiteDatabase database) {
                        new VideoDao(database).removeVideo(v.getId());
                    }
                });
                updateList();
                return true;
            }
        });
    }

    private void addNewVideo(String url) throws MalformedURLException {
        ParserTask pt = new ParserTask(new YoukuParser()){
            @Override
            protected void onPostExecute(AsyncResult<IParseResult, ParseException>[] asyncResults) {
                super.onPostExecute(asyncResults);
                final AsyncResult<IParseResult, ParseException> result = asyncResults[0];
                if(result.Exception == null){
                    DBHelper db = new DBHelper(VideoListActivity.this);
                    db.doWrite(new Effect<SQLiteDatabase>() {
                        @Override
                        public void e(SQLiteDatabase database) {
                            VideoDao dao = new VideoDao(database);
                            dao.addNewVideo(result.Result, albumName);
                        }
                    });
                    VideoListActivity.this.updateList();
                    Intent serv = new Intent(VideoListActivity.this, DownloadService.class);
                    startService(serv);
                } else {
                    Log.e(LOG_TAG,"Parse Error", result.Exception);
                    Toast.makeText(VideoListActivity.this,"youku视频解析错误",Toast.LENGTH_LONG).show();
                }
            }
        };
        IParser.ParserParam pp = new IParser.ParserParam(url, VideoDefinition.HIGH);
        pt.execute(pp);
    }

    private void askString(String title, final Effect<String> onOK){
        AlertDialog.Builder alert = new AlertDialog.Builder(this).setTitle(title);
        final EditText input = new EditText(this);
        alert.setView(input);
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                input.setText("canceled");
            }
        });
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onOK.e(input.getText().toString());
            }
        });
        alert.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        newVideoItem = menu.add("下载新视频");
        return true;
    }

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String name = null;

        getListView().setLongClickable(true);
        getListView().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Log.d(LOG_TAG,String.format("%s",view.getClass().getSimpleName()));
                return true;
            }
        });
        try {
            name = getIntent().getExtras().getString("album_name");
        } catch (NullPointerException ignored){
        }
        if(name!=null){
            albumName = name;
            Log.d(LOG_TAG,String.format("Album Name = %s",name));
            updateList();
        } else {
            finish();
        }
    }

    protected void updateList() {
        DBHelper helper = new DBHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        if(db!=null){
            try{
                VideoDao dao = new VideoDao(db);
                items = dao.listVideosByAlbum(albumName);
                this.setListAdapter(new VideoListViewAdapter(this,items));
                Log.d(LOG_TAG, String.format("Video Item Count %d",items.length));
            }finally {
                db.close();
            }
        }
    }

    @Override
    protected void onDownloadServiceStatusChange() {
        updateList();
    }
}