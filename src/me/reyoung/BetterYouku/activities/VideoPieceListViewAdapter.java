package me.reyoung.BetterYouku.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import me.reyoung.BetterYouku.R;
import me.reyoung.BetterYouku.util.DownloadImageTask;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午9:25
 */
public class VideoPieceListViewAdapter extends BaseAdapter {

    private final Context context;
    private final VideoPieceItem[] items;

    public static class VideoPieceItem {
        public int id;
        public String title;
        public String picUrl;
        public boolean isDownloaded;
        public String extName;
        public String albumName;
        public int vid;
    }



    public VideoPieceListViewAdapter(Context context, VideoPieceItem[] items){
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return items[i].id;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)  {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.album_list_item, parent, false);
        assert rowView != null;
        TextView name = (TextView) rowView.findViewById(R.id.name_text);
        name.setText(String.format("Part %d-%s",items[position].id+1, items[position].title));
        TextView desc = (TextView) rowView.findViewById(R.id.desc_text);
        desc.setText(items[position].isDownloaded?"已下载":"下载中");
        ImageView img = (ImageView) rowView.findViewById(R.id.album_image_view);
        new DownloadImageTask(img).execute(items[position].picUrl);
        return rowView;
    }
}
