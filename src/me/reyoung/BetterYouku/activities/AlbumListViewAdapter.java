package me.reyoung.BetterYouku.activities;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import me.reyoung.BetterYouku.R;
import me.reyoung.BetterYouku.util.DownloadImageTask;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午1:58
 */
public class AlbumListViewAdapter extends BaseAdapter {
    public static class AlbumListItem {
        public String name;
        public String desc;
        public String imgUrl;
    }
    private Context context;
    private AlbumListItem[] items;
    public AlbumListViewAdapter(Context context,AlbumListItem[] items) {
        this.context = context;
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.length;
    }

    @Override
    public Object getItem(int i) {
        return items[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.album_list_item, parent, false);
        assert rowView != null;
        TextView name = (TextView) rowView.findViewById(R.id.name_text);
        name.setText(items[position].name);
        TextView desc = (TextView) rowView.findViewById(R.id.desc_text);
        desc.setText(items[position].desc);
        ImageView img = (ImageView) rowView.findViewById(R.id.album_image_view);
        new DownloadImageTask(img).execute(items[position].imgUrl);
        return rowView;
    }

}
