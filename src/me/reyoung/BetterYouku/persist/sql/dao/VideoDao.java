package me.reyoung.BetterYouku.persist.sql.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import me.reyoung.BetterYouku.parser.IParseResult;
import me.reyoung.BetterYouku.persist.sql.DBHelper;
import me.reyoung.BetterYouku.util.DirectoryUtil;

import java.io.File;
import java.net.URL;
import java.util.Collection;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午3:50
 */
public class VideoDao {

    private static final String LOG_TAG = "VideoDao";
    private SQLiteDatabase database;

    public void addNewVideo(IParseResult result, String albumName) {
        Log.d(LOG_TAG,String.format("Add New Video %s",result.getTitle()));
        ContentValues values = new ContentValues();
        values.put("title", result.getTitle());
        values.put("album_name", albumName);
        values.put("image_url",result.getLogoUrl());

        int id  = (int) database.insert("Video", null,values);
        Collection<URL> urls = result.getDownloadUrls();
        int idx = 0;
        for(URL u: urls){
            values = new ContentValues();
            values.put("idx",idx++);
            values.put("videoUrl", u.toString());
            values.put("video_id", id);
            values.put("fileExtName", result.getFileExtName());
            database.insert("VideoItem", null, values);
        }
    }

    public void removeVideo(int id) {
        String idstr = String.valueOf(id);
        Cursor c  = database.rawQuery("SELECT v.album_name, vi.idx, vi.fileExtName FROM Video v JOIN VideoItem vi ON v.id = vi.video_id " +
                "WHERE v.id = ?",new String[]{idstr});
        for(;c.moveToNext();){
            File f = DirectoryUtil.getDownloadFileName(c.getString(0), id, c.getInt(1), c.getString(2));
            f.delete();
        }
        database.delete("VideoItem", "video_id = ?", new String[]{idstr});
        database.delete("Video", "id = ?", new String[]{idstr});
    }

    public static class VideoItem {
        public int id;
        public String title;
        public String imgUrl;
    }

    public VideoDao(SQLiteDatabase database){
        this.database = database;
    }

    public VideoItem[] listVideosByAlbum(String albumName){
        Cursor cursor = this.database.query("Video",new String[]{
                "id","title","image_url"
        },"album_name = ?",new String[]{albumName},null,null,"createTime desc");
        VideoItem [] retv = new VideoItem[cursor.getCount()];
        for(int i=0;cursor.moveToNext();++i){
            retv[i] = new VideoItem();
            retv[i].id = cursor.getInt(0);
            retv[i].title = cursor.getString(1);
            retv[i].imgUrl = cursor.getString(2);
        }
        return retv;
    }
}
