package me.reyoung.BetterYouku.persist.sql.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import me.reyoung.BetterYouku.util.DirectoryUtil;

import java.io.File;
import java.io.FileFilter;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午1:32
 */
public class AlbumDao {
    private SQLiteDatabase database;
    public AlbumDao(SQLiteDatabase database){
        this.database = database;
    }

    public void insertAlbum(String name, String desc, String url){
        ContentValues v = new ContentValues();
        v.put("name", name);
        v.put("description",desc);
        v.put("imageUrl", url);
        database.insert("Album",null, v);
        DirectoryUtil.getAppDir(name);
    }
}
