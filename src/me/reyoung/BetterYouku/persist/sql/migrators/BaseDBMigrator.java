package me.reyoung.BetterYouku.persist.sql.migrators;

import android.content.Context;
import android.content.res.Resources;
import android.database.sqlite.SQLiteDatabase;
import me.reyoung.BetterYouku.R;
import me.reyoung.BetterYouku.persist.sql.IDBMigrator;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午12:33
 */
public  class BaseDBMigrator implements IDBMigrator{
    private final int version;

    public BaseDBMigrator(int vid){
        version = vid;
    }

    @Override
    public String getCreateSQL(Context context) {
        String [] sqls = context.getResources().getStringArray(R.array.sql_create_init);
        return sqls[version -1];
    }

    @Override
    public void onAfterCreate(SQLiteDatabase ctx) {

    }

    @Override
    public void onBeforeCreate(SQLiteDatabase ctx) {
    }

    @Override
    public String getDropSQL(Context context) {
        String [] sqls = context.getResources().getStringArray(R.array.sql_drop);
        return sqls[version -1];
    }

    @Override
    public void onBeforeDrop(SQLiteDatabase ctx) {

    }

    @Override
    public void onAfterDrop(SQLiteDatabase ctx) {

    }
}
