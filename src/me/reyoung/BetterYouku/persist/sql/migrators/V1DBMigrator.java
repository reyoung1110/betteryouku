package me.reyoung.BetterYouku.persist.sql.migrators;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import me.reyoung.BetterYouku.persist.sql.dao.AlbumDao;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午1:24
 */
public class V1DBMigrator extends BaseDBMigrator {
    private static final String LOG_TAG = "V1DBMigrator";


    public V1DBMigrator() {
        super(1);
    }

    @Override
    public void onAfterCreate(SQLiteDatabase ctx) {
        super.onAfterCreate(ctx);
        // Insert DEFAULT Album.
        Log.d(LOG_TAG, "On After Create");
        new AlbumDao(ctx).insertAlbum("默认列表","默认的下载列表","http://img2.imgtn.bdimg.com/it/u=885593490,537157646&fm=21&gp=0.jpg");
    }
}
