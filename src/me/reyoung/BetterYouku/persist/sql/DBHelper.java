package me.reyoung.BetterYouku.persist.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import fj.Effect;
import fj.F;
import fj.F2;
import me.reyoung.BetterYouku.persist.sql.migrators.BaseDBMigrator;
import me.reyoung.BetterYouku.persist.sql.migrators.V1DBMigrator;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 上午2:11
 */
public class DBHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "database.db";
    private Context context;
    public IDBMigrator[] migrators = {
            new V1DBMigrator(),
            new BaseDBMigrator(2),
            new BaseDBMigrator(3),
            new BaseDBMigrator(4)
    };


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    public <T> T  doWrite  (F<SQLiteDatabase, T> func){
        SQLiteDatabase db = getWritableDatabase();
        if(db!=null){
            try {
                return func.f(db);
            }finally {
                db.close();
            }
        }
        return null;
    }

    public void doWrite(Effect<SQLiteDatabase> func){
        SQLiteDatabase db = getWritableDatabase();
        if(db!=null){
            try {
                func.e(db);
            }finally {
                db.close();
            }
        }
    }

    public void doRead(Effect<SQLiteDatabase> func){
        SQLiteDatabase db = getReadableDatabase();
        if(db!=null){
            try{
                func.e(db);
            }finally {
                db.close();
            }
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for(int i=0;i<DATABASE_VERSION;++i){
            migrators[i].onBeforeCreate(sqLiteDatabase);
            sqLiteDatabase.execSQL(migrators[i].getCreateSQL(context));
            migrators[i].onAfterCreate(sqLiteDatabase);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        for(;i<i2;++i){
            migrators[i].onBeforeCreate(sqLiteDatabase);
            sqLiteDatabase.execSQL(migrators[i].getCreateSQL(context));
            migrators[i].onAfterCreate(sqLiteDatabase);
        }
    }
}
