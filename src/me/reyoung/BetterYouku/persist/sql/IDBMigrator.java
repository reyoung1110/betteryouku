package me.reyoung.BetterYouku.persist.sql;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * User: reyoung
 * Date: 14-5-21
 * Time: 下午12:30
 */
public interface IDBMigrator {
    public String getCreateSQL(Context context);
    public void   onAfterCreate(SQLiteDatabase ctx);
    public void   onBeforeCreate(SQLiteDatabase ctx);
    public String getDropSQL(Context context);
    public void   onBeforeDrop(SQLiteDatabase ctx);
    public void   onAfterDrop(SQLiteDatabase ctx);
}
